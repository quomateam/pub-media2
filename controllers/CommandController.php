<?php

namespace quoma\media\controllers;


use quoma\media\models\Sized;
use yii\console\Controller;

class CommandController extends Controller
{
    public function actionRemoveAllSized()
    {
        echo "Iniciando...\n";
        $count = 0;

        foreach(Sized::find()->each() as $sized){
            $sized->delete();
            $count++;

            if($count%100 == 0){
                echo "$count\n";
            }
        }

        echo "Total: $count \n";
    }
}