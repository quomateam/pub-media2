<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace quoma\media\models\types;

use hosanna\audiojs\AudioJs;
use quoma\media\components\upload\UploadWidget;
use quoma\media\models\Media;
use Yii;

/**
 * Description of Audio
 *
 * @author mmoyano
 */
class SVG extends Media
{
    
    public function init()
    {
        parent::init();
        $this->type = 'SVG';
    }
    
    public function rules()
    {
        
        $rules = parent::rules();
        $rules[] = ['file', 'file',
            'extensions' => 'svg',
        ];
        
        return $rules;
    }
    
    public function render($width = null, $height = null, $options = [])
    {
        return \yii\helpers\Html::img($this->getUrl(), $options);
    }

    public function renderPreview()
    {
        return \yii\helpers\Html::tag('div',
            \yii\helpers\Html::img($this->getUrl(), [
                'style' => 'width: 300px'
            ]),
            [
                'style' => 'background-color: #888'
            ]);
    }
    
    public function renderButton($options = [], $params = []) {
        return UploadWidget::widget([
            'type' => 'svg',
            'label' => '<span class="glyphicon glyphicon-plus"></span> '.Yii::t('app', 'SVG'),
            'buttonOptions' => $options,
            'template' => '{input}',
            'extraParams' => $params
        ]);
    }
    
}
