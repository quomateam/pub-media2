<?php

use yii\db\Migration;

/**
 * Class m190925_192918_sized_parent
 */
class m190925_192918_sized_parent extends Migration
{
    public function init(){
        $this->db = 'db_media';
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sized', 'parent_id', $this->integer()->null());
        $this->addColumn('sized', 'garbage', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('sized', 'parent_id');
        $this->dropColumn('sized', 'garbage');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190925_192918_sized_parent cannot be reverted.\n";

        return false;
    }
    */
}
